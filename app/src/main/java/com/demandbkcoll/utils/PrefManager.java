package com.demandbkcoll.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public Context context;

    //Constructor
    public PrefManager(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
    }

    public void clearPrefByKey(String Key) {
        editor = sharedPreferences.edit();
        editor.remove(Key).apply();
    }

    public void setMobile(String mobile) {
        editor = sharedPreferences.edit();
        editor.putString("MobileNo", mobile);
        editor.apply();
    }

    public String getMobile() {
        return sharedPreferences.getString("MobileNo", "");
    }

    public void setId(String id) {
        editor = sharedPreferences.edit();
        editor.putString("Id", id);
        editor.apply();
    }

    public String getId() {
        return sharedPreferences.getString("Id", "");
    }

    public void setLoginType(String loginType) {
        editor = sharedPreferences.edit();
        editor.putString("LoginType", loginType);
        editor.apply();
    }

    public String getLoginType() {
        return sharedPreferences.getString("LoginType", "");
    }

    public void setLogin(String isLogin) {
        editor = sharedPreferences.edit();
        editor.putString("IsLogin", isLogin);
        editor.apply();
    }

    public String isLogin() {
        return sharedPreferences.getString("IsLogin", "False");
    }

    public void setInputType(String loginType) {
        editor = sharedPreferences.edit();
        editor.putString("InputType", loginType);
        editor.apply();
    }

    public String getInputType() {
        return sharedPreferences.getString("InputType", "");
    }

    public void setFirstTime(String firstTime) {
        editor = sharedPreferences.edit();
        editor.putString("FirstTime", firstTime);
        editor.apply();
    }

    public String isFirstTime() {
        return sharedPreferences.getString("FirstTime", "False");
    }
}

package com.demandbkcoll.utils;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class Utils {

    //Method : Check Internet
    public static boolean isOnline(Context context) {
        if (context != null) {
            ConnectivityManager connMgr = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connMgr != null) {
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                return (networkInfo != null && networkInfo.isConnected());
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //Method : Check String Empty
    public static boolean isEmpty(String string) {
        if (TextUtils.isEmpty(string) || string.equals(AppConfig.KEY_EMPTY_STRING) || string.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    //Method : Open Keyboard
    public static void openKeyboard(Context context, View view) {

        if (context != null) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.toggleSoftInputFromWindow(
                        view.getApplicationWindowToken(),
                        InputMethodManager.SHOW_FORCED, 0);
            }
        }
    }

    //Method : MonthName
    public static String getMonthName(String s) {
        switch (s) {
            case "1":
                s = "Jan";
                break;
            case "2":
                s = "Feb";
                break;
            case "3":
                s = "Mar";
                break;
            case "4":
                s = "Apr";
                break;
            case "5":
                s = "May";
                break;
            case "6":
                s = "Jun";
                break;
            case "7":
                s = "Jul";
                break;
            case "8":
                s = "Aug";
                break;
            case "9":
                s = "Sep";
                break;
            case "10":
                s = "Oct";
                break;
            case "11":
                s = "Nov";
                break;
            case "12":
                s = "Dec";
                break;
        }
        return s;
    }

    //Method : Hide Keyboard
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //Method : Convert First Later Capital
    public static String convertFirstLetterCap(String str) {

        // Create a char array of given String
        char ch[] = str.toCharArray();
        for (int i = 0; i < str.length(); i++) {

            // If first character of a word is found
            if (i == 0 && ch[i] != ' ' ||
                    ch[i] != ' ' && ch[i - 1] == ' ') {

                // If it is in lower-case
                if (ch[i] >= 'a' && ch[i] <= 'z') {

                    // Convert into Upper-case
                    ch[i] = (char) (ch[i] - 'a' + 'A');
                }
            }

            // If apart from first character
            // Any one is in Upper-case
            else if (ch[i] >= 'A' && ch[i] <= 'Z')

                // Convert into Lower-Case
                ch[i] = (char) (ch[i] + 'a' - 'A');
        }

        // Convert the char array to equivalent String
        String st = new String(ch);
        return st;
    }

    //Method : Blink
    public static void blink(final TextView txt) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int timeToBlink = 50;    //in milissegunds
                try {
                    Thread.sleep(timeToBlink);
                } catch (Exception e) {
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (txt.getVisibility() == View.VISIBLE) {
                            txt.setVisibility(View.INVISIBLE);
                        } else {
                            txt.setVisibility(View.VISIBLE);
                        }
                        blink(txt);
                    }
                });
            }
        }).start();
    }

    public static String parseError(String errorJson, String[] keys) {

        try {

            String error = "";

            JSONObject jObjError = new JSONObject(errorJson);
            JSONObject j = jObjError.getJSONObject("error");

            for (String key : keys) {
                if (j.has(key)) {
                    error = j.getString(key);
                    break;
                }
            }

            return error;

        } catch (JSONException e) {

            e.printStackTrace();
            return e.getLocalizedMessage();
        }
    }

    public static void hideStatusBar(Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            View decorView = activity.getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            ActionBar actionBar = activity.getActionBar();
            if (actionBar != null) {
                actionBar.hide();
            }
        }
    }

}

package com.demandbkcoll.utils;

public class AppConfig {
    public static final String KEY_EMPTY_STRING = "";
    public static final String KEY_GRANT_PERMISSION = "Please, Grant Permission !!";
    public static final String KEY_BACK = "Back";
    public static final String KEY_GO_SETTING = "Go to settings";
    public static final String KEY_PKG = "package";

    ///////////////////////////////////////////////////////////////////////////
    // Base URLs
    ///////////////////////////////////////////////////////////////////////////

    //URL
    public static final String BASE_URL = "http://206.189.132.136/api/public/api/";



    ///////////////////////////////////////////////////////////////////////////
    // APIs
    ///////////////////////////////////////////////////////////////////////////
    public static final String API_LOGIN = "register";
    public static final String API_VERSION = "app_version";
    public static final String API_LOGIN_VOL = "login";
    public static final String API_CITY = "city_list";
    public static final String API_STORE = "get_stores/";
    public static final String API_MEDICAL = "get_medicals/";
    public static final String API_GET_REQS = "get_requests/";
    public static final String API_GET_ELE = "get_electrician/";
    public static final String API_GET_PLUM = "get_plumbers/";
    public static final String API_GET_DOC = "get_doctors/";
    public static final String API_INSERT_REQ = "send_request";
    public static final String API_OLD_HELPERS = "get_old_helpers";
    public static final String API_JATI = "get_rural";
    public static final String API_UPDATE_REQ = "complete_request";
    public static final String API_GET_DIST_DOCTORS = "get_district_doctors";
    public static final String API_INSERT_DATA = "call_history";
}

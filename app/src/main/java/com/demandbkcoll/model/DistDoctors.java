package com.demandbkcoll.model;

public class DistDoctors {

    private String id;
    private String name;
    private String contact;
    private String type;

    public DistDoctors(String id, String name, String contact, String type) {
        this.id = id;
        this.name = name;
        this.contact = contact;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getContact() {
        return contact;
    }

    public String getType() {
        return type;
    }
}

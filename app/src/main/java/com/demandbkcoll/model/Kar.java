package com.demandbkcoll.model;

public class Kar {

    private String id;
    private String storeName;
    private String owner1;
    private String owner2;
    private String address;
    private String city;
    private String cityId;
    private String mobile1;
    private String mobile2;

    public Kar(String id, String storeName, String owner1, String owner2, String address,
               String city, String cityId, String mobile1, String mobile2) {
        this.id = id;
        this.storeName = storeName;
        this.owner1 = owner1;
        this.owner2 = owner2;
        this.address = address;
        this.city = city;
        this.cityId = cityId;
        this.mobile1 = mobile1;
        this.mobile2 = mobile2;
    }

    public String getId() {
        return id;
    }

    public String getStoreName() {
        return storeName;
    }

    public String getOwner1() {
        return owner1;
    }

    public String getOwner2() {
        return owner2;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getCityId() {
        return cityId;
    }

    public String getMobile1() {
        return mobile1;
    }

    public String getMobile2() {
        return mobile2;
    }
}

package com.demandbkcoll.model;

public class Vollist {

    private String id;
    private String userId;
    private String name;
    private String requestType;
    private String description;
    private String contactNo;
    private String noOfPeople;
    private String completed;


    public Vollist(String id, String userId, String name, String requestType, String description, String contactNo, String noOfPeople, String completed) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.requestType = requestType;
        this.description = description;
        this.contactNo = contactNo;
        this.noOfPeople = noOfPeople;
        this.completed = completed;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getRequestType() {
        return requestType;
    }

    public String getDescription() {
        return description;
    }

    public String getContactNo() {
        return contactNo;
    }

    public String getNoOfPeople() {
        return noOfPeople;
    }

    public String getCompleted() {
        return completed;
    }
}

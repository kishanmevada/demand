package com.demandbkcoll.model;

public class Vadil {

    private String name;
    private String mobile;

    public Vadil(String name, String mobile) {
        this.name = name;
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public String getMobile() {
        return mobile;
    }
}

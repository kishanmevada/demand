package com.demandbkcoll.interfaces;

public interface CityInterface {
    void onCitySelected(String cityId, String cityName);
}

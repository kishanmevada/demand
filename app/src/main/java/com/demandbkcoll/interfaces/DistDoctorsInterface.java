package com.demandbkcoll.interfaces;

public interface DistDoctorsInterface {
    void onCall(String mobile1, String mobile2, String storeTitle, String cityId);
}

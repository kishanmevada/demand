package com.demandbkcoll.interfaces;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

import static com.demandbkcoll.utils.AppConfig.API_CITY;
import static com.demandbkcoll.utils.AppConfig.API_GET_DIST_DOCTORS;
import static com.demandbkcoll.utils.AppConfig.API_GET_DOC;
import static com.demandbkcoll.utils.AppConfig.API_GET_ELE;
import static com.demandbkcoll.utils.AppConfig.API_GET_PLUM;
import static com.demandbkcoll.utils.AppConfig.API_GET_REQS;
import static com.demandbkcoll.utils.AppConfig.API_INSERT_DATA;
import static com.demandbkcoll.utils.AppConfig.API_INSERT_REQ;
import static com.demandbkcoll.utils.AppConfig.API_JATI;
import static com.demandbkcoll.utils.AppConfig.API_LOGIN;
import static com.demandbkcoll.utils.AppConfig.API_LOGIN_VOL;
import static com.demandbkcoll.utils.AppConfig.API_MEDICAL;
import static com.demandbkcoll.utils.AppConfig.API_OLD_HELPERS;
import static com.demandbkcoll.utils.AppConfig.API_STORE;
import static com.demandbkcoll.utils.AppConfig.API_UPDATE_REQ;
import static com.demandbkcoll.utils.AppConfig.API_VERSION;

public interface ApiInterface {


    //API : Standard
    @GET(API_VERSION)
    Call<ResponseBody> getVersion();

    //API : Standard
    @GET(API_CITY)
    Call<ResponseBody> getCities();

    //API : Standard
    @GET(API_OLD_HELPERS)
    Call<ResponseBody> getVadilList();

    //API : Standard
    @GET(API_GET_DIST_DOCTORS)
    Call<ResponseBody> getDistDoctors();

    //API : Standard
    @GET(API_JATI)
    Call<ResponseBody> getJatiList();


    //API : Login
    @FormUrlEncoded
    @POST(API_LOGIN)
    Call<ResponseBody> login(
            @Field("mobile") String userName
    );

    //API : Update Status
    @FormUrlEncoded
    @POST(API_UPDATE_REQ)
    Call<ResponseBody> updateStatus(
            @Field("id") String id
    );

    //API : Login
    @FormUrlEncoded
    @POST(API_LOGIN_VOL)
    Call<ResponseBody> loginVol(
            @Field("mobile") String userName,
            @Field("password") String password
    );

    //API : Login
    @FormUrlEncoded
    @POST(API_INSERT_REQ)
    Call<ResponseBody> insertReq(
            @Field("user_id") String userId,
            @Field("request_type") String reqType,
            @Field("description") String descriptionAndNotes,
            @Field("contact_no") String contact,
            @Field("no_of_people") String noOfPeople,
            @Field("name") String name,
            @Field("city_id") String cityId
    );

    //API : Insert Data
    @FormUrlEncoded
    @POST(API_INSERT_DATA)
    Call<ResponseBody> insertData(
            @Field("user_id") String userId,
            @Field("title") String title,
            @Field("contact_no") String contactNo,
            @Field("type") String type,
            @Field("city_id") String cityId
    );

    //API : School
    @GET (API_STORE+"{city_id}")
    Call<ResponseBody> getStore(@Path("city_id") String cityId);

    //API : School
    @GET (API_MEDICAL+"{city_id}")
    Call<ResponseBody> getMedical(@Path("city_id") String cityId);

    //API : School
    @GET (API_GET_REQS+"{city_id}")
    Call<ResponseBody> getReqs(@Path("city_id") String cityId);

    //API : School
    @GET (API_GET_ELE+"{city_id}")
    Call<ResponseBody> getEle(@Path("city_id") String cityId);

    //API : School
    @GET (API_GET_PLUM+"{city_id}")
    Call<ResponseBody> getPlum(@Path("city_id") String cityId);

    //API : Doc
    @GET (API_GET_DOC+"{city_id}")
    Call<ResponseBody> getDoctors(@Path("city_id") String cityId);

    /*//API : Year
    @GET(API_PASS_OUT_YEAR)
    Call<ResponseBody> getPassoutYear();

    //API : Country
    @GET("countries")
    Call<ResponseBody> getCountry();

    //API : States
    @GET(API_STATE+"{id}")
    Call<ResponseBody> getStates(@Path("id") String id);


    //API : District
    @GET(API_DIST+"{id}")
    Call<ResponseBody> getDist(@Path("id") String id);

    //API : City
    @GET(API_CITY+"{id}")
    Call<ResponseBody> getCity(@Path("id") String id);

    //API : School
    @GET(API_SCHOOL+"{id}")
    Call<ResponseBody> getSchool(@Path("id") String id);

    //API : Login
    @FormUrlEncoded
    @POST(API_LOGIN)
    Call<ResponseBody> login(
            @Field("username") String userName,
            @Field("password") String password
    );

    //API : Community
    @GET("communities")
    Call<ResponseBody> getCommunity();

    //API : Register
    @FormUrlEncoded
    @POST("register")
    Call<ResponseBody> register(
            @Field("name") String name,
            @Field("phone") String phone,
            @Field("username") String userName,
            @Field("password") String password,
            @Field("school_id") String schoolId,
            @Field("standard_id") String standardId,
            @Field("batch_id") String batchId
    );

    @Multipart
    @POST(AppConfig.API_UPLOAD_IMAGE)
    Call<ResponseBody> uploadImage(@Part MultipartBody.Part file);*/
}

package com.demandbkcoll.adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.demandbkcoll.R;
import com.demandbkcoll.interfaces.MedicalInterface;
import com.demandbkcoll.model.Medical;

import java.util.List;

public class MedicalAdapter extends RecyclerView.Adapter<MedicalAdapter.VhMedical> {

    private Context context;
    private List<Medical> data;
    private MedicalInterface karInterface;

    public MedicalAdapter(Context context, List<Medical> data, MedicalInterface karInterface) {
        this.context = context;
        this.data = data;
        this.karInterface = karInterface;
    }

    @NonNull
    @Override
    public VhMedical onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_medical, parent, false);
        return new VhMedical(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VhMedical holder, final int position) {

        //Store Name
        if (!TextUtils.isEmpty(data.get(position).getStoreName())) {
            holder.lblStoreName.setText(data.get(position).getStoreName());
        }

        //Owner 1
        if (!TextUtils.isEmpty(data.get(position).getOwner1())) {

            holder.linearOwner1.setVisibility(View.VISIBLE);
            holder.lblOwner1.setText(data.get(position).getOwner1());

            if (!TextUtils.isEmpty(data.get(position).getMobile1())) {

                holder.linearCall1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        karInterface.onCall(
                                data.get(position).getMobile1(),
                                data.get(position).getMobile2(),
                                data.get(position).getStoreName(),
                                data.get(position).getCityId());
                    }
                });
            }
        } else {
            holder.linearOwner1.setVisibility(View.GONE);
        }

        //Owner 2
        if (!TextUtils.isEmpty(data.get(position).getOwner2())) {

            holder.linearOwner2.setVisibility(View.VISIBLE);
            holder.lblOwner2.setText(data.get(position).getOwner2());

            if (!TextUtils.isEmpty(data.get(position).getMobile2())) {

                holder.linarCall2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        karInterface.onCall(
                                data.get(position).getMobile1(),
                                data.get(position).getMobile2(),
                                data.get(position).getStoreName(),
                                data.get(position).getCityId());
                    }
                });
            }
        } else {
            holder.linearOwner2.setVisibility(View.GONE);
        }

        //Address
        if (!TextUtils.isEmpty(data.get(position).getAddress())) {
            holder.lblAddress.setText(data.get(position).getAddress());
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class VhMedical extends RecyclerView.ViewHolder {

        private TextView lblStoreName;
        private TextView lblOwner1;
        private TextView lblOwner2;
        private TextView lblAddress;
        private ImageView imgCall1;
        private ImageView imgCall2;
        private LinearLayout linearOwner1;
        private LinearLayout linearOwner2;
        private LinearLayout linearCall1;
        private LinearLayout linarCall2;

        @SuppressLint("CutPasteId")
        VhMedical(@NonNull View itemView) {
            super(itemView);

            lblStoreName = itemView.findViewById(R.id.lblStoreNameItemMedical);
            lblOwner1 = itemView.findViewById(R.id.lblOwner1ItemMedical);
            lblOwner2 = itemView.findViewById(R.id.lblOwner2ItemMedical);
            lblAddress = itemView.findViewById(R.id.lblAddressItemMedical);
            imgCall1 = itemView.findViewById(R.id.imgCallOwner1ItemMedical);
            linearCall1 = itemView.findViewById(R.id.linearCallOwner1ItemMedical);
            imgCall2 = itemView.findViewById(R.id.imgCallOwner2ItemMedical);
            linarCall2 = itemView.findViewById(R.id.linearCallOwner2ItemMedical);
            linearOwner1 = itemView.findViewById(R.id.linearOwner1ItemMedical);
            linearOwner2 = itemView.findViewById(R.id.linearOwner2ItemMedical);

        }

    }
}

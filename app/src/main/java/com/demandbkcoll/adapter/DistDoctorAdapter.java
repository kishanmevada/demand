package com.demandbkcoll.adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.demandbkcoll.R;
import com.demandbkcoll.interfaces.DistDoctorsInterface;
import com.demandbkcoll.model.DistDoctors;

import java.util.List;

public class DistDoctorAdapter extends RecyclerView.Adapter<DistDoctorAdapter.VhDistDoctors> {

    private Context context;
    private List<DistDoctors> data;
    private DistDoctorsInterface karInterface;

    public DistDoctorAdapter(Context context, List<DistDoctors> data, DistDoctorsInterface karInterface) {
        this.context = context;
        this.data = data;
        this.karInterface = karInterface;
    }

    @NonNull
    @Override
    public VhDistDoctors onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_dist_doct, parent, false);
        return new VhDistDoctors(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VhDistDoctors holder, final int position) {

        //Owner 1
        if (!TextUtils.isEmpty(data.get(position).getName())) {

            //Name
            if (!TextUtils.isEmpty(data.get(position).getName())) {
                holder.lblName.setText(data.get(position).getName());
            }

            //Type
            if (!TextUtils.isEmpty(data.get(position).getType())) {
                holder.lblType.setText(data.get(position).getType());
            }

            if (!TextUtils.isEmpty(data.get(position).getContact())) {

                holder.linearCall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        karInterface.onCall(
                                data.get(position).getContact(),
                                data.get(position).getContact(),
                                data.get(position).getName(),
                                "3");
                    }
                });
            }
        } else {
            holder.itemView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class VhDistDoctors extends RecyclerView.ViewHolder {

        private TextView lblName;
        private TextView lblType ;
        private LinearLayout linearCall ;

        @SuppressLint("CutPasteId")
        VhDistDoctors(@NonNull View itemView) {
            super(itemView);
            lblName = itemView.findViewById(R.id.lblNameItemDistDoct);
            lblType = itemView.findViewById(R.id.lblTypeItemDistDoct);
            linearCall = itemView.findViewById(R.id.linearCallItemDistDoct);
        }
    }
}

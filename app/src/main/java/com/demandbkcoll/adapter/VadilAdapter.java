package com.demandbkcoll.adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.demandbkcoll.R;
import com.demandbkcoll.interfaces.VadilInterface;
import com.demandbkcoll.model.Vadil;

import java.util.List;

public class VadilAdapter extends RecyclerView.Adapter<VadilAdapter.VhVadil> {

    private Context context;
    private List<Vadil> data;
    private VadilInterface karInterface;

    public VadilAdapter(Context context, List<Vadil> data, VadilInterface karInterface) {
        this.context = context;
        this.data = data;
        this.karInterface = karInterface;
    }

    @NonNull
    @Override
    public VhVadil onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_vadil, parent, false);
        return new VhVadil(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VhVadil holder, final int position) {

        //Owner 1
        if (!TextUtils.isEmpty(data.get(position).getName())) {

            //Store Name
            if (!TextUtils.isEmpty(data.get(position).getName())) {
                holder.lblName.setText(data.get(position).getName());
            }

            if (!TextUtils.isEmpty(data.get(position).getMobile())) {

                holder.linearCall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        karInterface.onCall(
                                data.get(position).getMobile(),
                                data.get(position).getMobile(),
                                data.get(position).getName(),
                                "3");
                    }
                });
            }
        } else {
            holder.itemView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class VhVadil extends RecyclerView.ViewHolder {

        private TextView lblName;
        private LinearLayout linearCall ;

        @SuppressLint("CutPasteId")
        VhVadil(@NonNull View itemView) {
            super(itemView);
            lblName = itemView.findViewById(R.id.lblNameItemVadil);
            linearCall = itemView.findViewById(R.id.linearCallItemValdil);
        }
    }
}

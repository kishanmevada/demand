package com.demandbkcoll.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.demandbkcoll.R;
import com.demandbkcoll.interfaces.KarInterface;
import com.demandbkcoll.model.Kar;

import java.util.List;

public class KarAdapter extends RecyclerView.Adapter<KarAdapter.VhKar> {

    private Context context;
    private List<Kar> data;
    private KarInterface karInterface;

    public KarAdapter(Context context, List<Kar> data, KarInterface karInterface) {
        this.context = context;
        this.data = data;
        this.karInterface = karInterface;
    }

    @NonNull
    @Override
    public VhKar onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_kar, parent, false);
        return new VhKar(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VhKar holder, final int position) {

        //Store Name
        if (!TextUtils.isEmpty(data.get(position).getStoreName())) {
            holder.lblStoreName.setText(data.get(position).getStoreName());
        }

        //Owner 1
        if (!TextUtils.isEmpty(data.get(position).getOwner1())) {

            holder.linearOwner1.setVisibility(View.VISIBLE);
            holder.lblOwner1.setText(data.get(position).getOwner1());

            if (!TextUtils.isEmpty(data.get(position).getMobile1())) {

                holder.linearCall1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        karInterface.onCall(
                                data.get(position).getMobile1(),
                                data.get(position).getMobile2(),
                                data.get(position).getStoreName(),
                                data.get(position).getCityId());
                    }
                });
            }
        } else {
            holder.linearOwner1.setVisibility(View.GONE);
        }

        //Owner 2
        if (!TextUtils.isEmpty(data.get(position).getOwner2())) {

            holder.linearOwner2.setVisibility(View.VISIBLE);
            holder.lblOwner2.setText(data.get(position).getOwner2());

            if (!TextUtils.isEmpty(data.get(position).getMobile2())) {

                holder.linearCall2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        karInterface.onCall(
                                data.get(position).getMobile1(),
                                data.get(position).getMobile2(),
                                data.get(position).getStoreName(),
                                data.get(position).getCityId()
                        );
                    }
                });
            }
        } else {
            holder.linearOwner2.setVisibility(View.GONE);
        }

        //Address
        if (!TextUtils.isEmpty(data.get(position).getAddress())) {
            holder.lblAddress.setText(data.get(position).getAddress());
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class VhKar extends RecyclerView.ViewHolder {

        private TextView lblStoreName;
        private TextView lblOwner1;
        private TextView lblOwner2;
        private TextView lblAddress;
        private ImageView imgCall1;
        private ImageView imgCall2;
        private LinearLayout linearOwner1;
        private LinearLayout linearOwner2;
        private LinearLayout linearCall1;
        private LinearLayout linearCall2;

        VhKar(@NonNull View itemView) {
            super(itemView);

            lblStoreName = itemView.findViewById(R.id.lblStoreNameItemKar);
            lblOwner1 = itemView.findViewById(R.id.lblOwner1ItemKar);
            lblOwner2 = itemView.findViewById(R.id.lblOwner2ItemKar);
            lblAddress = itemView.findViewById(R.id.lblAddressItemKar);
            imgCall1 = itemView.findViewById(R.id.imgCallOwner1ItemKar);
            linearCall1 = itemView.findViewById(R.id.linearCallOwner1ItemKar);
            imgCall2 = itemView.findViewById(R.id.imgCallOwner2ItemKar);
            linearCall2 = itemView.findViewById(R.id.linearCallOwner2ItemKar);
            linearOwner1 = itemView.findViewById(R.id.linearOwner1ItemKar);
            linearOwner2 = itemView.findViewById(R.id.linearOwner2ItemKar);

        }

    }
}

package com.demandbkcoll.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.demandbkcoll.R;
import com.demandbkcoll.interfaces.CityInterface;
import com.demandbkcoll.model.City;
import java.util.List;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.VhCity> {

    private Context context;
    private List<City> data;
    private CityInterface cityInterface;

    public CityAdapter(Context context, List<City> data, CityInterface cityInterface) {
        this.context = context;
        this.data = data;
        this.cityInterface = cityInterface;
    }

    @NonNull
    @Override
    public VhCity onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_city, parent, false);
        return new VhCity(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VhCity holder, final int position) {

        //Store Name
        if (!TextUtils.isEmpty(data.get(position).getCityName())) {
            holder.lblStoreName.setText(data.get(position).getCityName());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cityInterface.onCitySelected(
                            data.get(position).getCityId(),
                            data.get(position).getCityName()
                    );
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class VhCity extends RecyclerView.ViewHolder {

        private TextView lblStoreName;

        VhCity(@NonNull View itemView) {
            super(itemView);

            lblStoreName = itemView.findViewById(R.id.lblCityNameItemCity);

        }

    }
}

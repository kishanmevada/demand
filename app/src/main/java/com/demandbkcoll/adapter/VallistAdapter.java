package com.demandbkcoll.adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.demandbkcoll.R;
import com.demandbkcoll.interfaces.VolInterface;
import com.demandbkcoll.model.Vollist;

import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;

public class VallistAdapter extends RecyclerView.Adapter<VallistAdapter.VhVollist> {

    private Context context;
    private List<Vollist> data;
    private VolInterface vollistInterface;

    public VallistAdapter(Context context, List<Vollist> data, VolInterface vollistInterface) {
        this.context = context;
        this.data = data;
        this.vollistInterface = vollistInterface;
    }

    @NonNull
    @Override
    public VhVollist onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_vol_list, parent, false);
        return new VhVollist(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull VhVollist holder, final int position) {


        //Total
        if (!TextUtils.isEmpty(data.get(position).getNoOfPeople())) {
            holder.lblTotal.setText("લોકોની સંખ્યા : "+data.get(position).getNoOfPeople());
        }

        //Name
        if (!TextUtils.isEmpty(data.get(position).getName())) {
            holder.lblName.setText(data.get(position).getName());
        }

        //Address
        if (!TextUtils.isEmpty(data.get(position).getDescription())) {
            holder.lblAddress.setText(data.get(position).getDescription());
        }

        //Call
        if (!TextUtils.isEmpty(data.get(position).getContactNo())) {

            holder.lblNumber.setText(data.get(position).getContactNo());

            holder.linearCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    String mobile = data.get(position).getContactNo();
                    callIntent.setData(Uri.parse("tel:" + mobile));
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE)
                            == PackageManager.PERMISSION_GRANTED) {
                        context.startActivity(callIntent);
                    } else {
                        if (context != null) {
                            Toast.makeText(context, "Please, Grant Permission For Call in settings", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }

        //Listener --> Done
        holder.lblDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vollistInterface.onDone(data.get(position).getId());
            }
        });

        //Listener --> Number
        holder.lblNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(data.get(position).getContactNo())) {
                    if (context != null) {
                        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("label", data.get(position).getContactNo());
                        if (clipboard != null) {
                            clipboard.setPrimaryClip(clip);
                            Toast.makeText(context,  data.get(position).getContactNo() + " copied !!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class VhVollist extends RecyclerView.ViewHolder {

        private TextView lblTotal;
        private TextView lblName;
        private TextView lblAddress;
        private TextView lblDone;
        private TextView lblNumber;
        private ImageView imgCall;
        private LinearLayout linearCall;

        VhVollist(@NonNull View itemView) {
            super(itemView);

            lblNumber = itemView.findViewById(R.id.lblMobileItemVallist);
            lblTotal = itemView.findViewById(R.id.lblTotalItemVallist);
            lblDone = itemView.findViewById(R.id.lblDone);
            lblName = itemView.findViewById(R.id.lblNameItemVallist);
            lblAddress = itemView.findViewById(R.id.lblAddressItemVallist);
            imgCall = itemView.findViewById(R.id.imgCallItemVallist);
            linearCall = itemView.findViewById(R.id.linearCallItemVallist);
        }
    }
}

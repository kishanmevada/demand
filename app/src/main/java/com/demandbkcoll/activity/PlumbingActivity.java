package com.demandbkcoll.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.demandbkcoll.R;
import com.demandbkcoll.adapter.MedicalAdapter;
import com.demandbkcoll.api.APIClient;
import com.demandbkcoll.bottomsheet.CityFragment;
import com.demandbkcoll.databinding.ActivityMedicalBinding;
import com.demandbkcoll.databinding.ActivityPlumbingBinding;
import com.demandbkcoll.interfaces.ApiInterface;
import com.demandbkcoll.interfaces.CityInterface;
import com.demandbkcoll.interfaces.MedicalInterface;
import com.demandbkcoll.model.City;
import com.demandbkcoll.model.Medical;
import com.demandbkcoll.utils.AppConfig;
import com.demandbkcoll.utils.ConnectionDetector;
import com.demandbkcoll.utils.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlumbingActivity extends AppCompatActivity {

    private static final String TAG = "PlumbingActivity";

    private static final int PERMISSION_REQUEST_CODE = 10;

    private static final float maxHeight = 1280.0f;
    private static final float maxWidth = 1280.0f;

    private AlertDialog aDialog;
    private ActivityPlumbingBinding a;
    private PrefManager prefManager;

    private List<Medical> listMedical = new ArrayList<>();
    private List<City> listCity = new ArrayList<>();

    String[] appPermissions = {Manifest.permission.CALL_PHONE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plumbing);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        a = DataBindingUtil.setContentView(this, R.layout.activity_plumbing);

        if (checkAndRequestPermission()) {
            init();
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Override Methods
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == PERMISSION_REQUEST_CODE) {

            //Init
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;

            //Gather permission grant Result
            for (int i = 0; i < grantResults.length; i++) {

                //Add Only denied permission
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }

            }

            //Check if all permission are granted
            if (deniedCount == 0) {

                //Allow to proceed
                init();
            } else {  //At least one or all permission are are denied

                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {

                    String permName = entry.getKey();

                    //Permission is denied and never ask again isn't checked
                    //ask again explaining the importance of permission
                    //shouldShowRequestPermissionRationale will true
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {

                        showPermissionDialog(AppConfig.KEY_EMPTY_STRING,
                                "App need your permission to process further",
                                AppConfig.KEY_GRANT_PERMISSION,
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        aDialog.dismiss();
                                        checkAndRequestPermission();
                                    }
                                },
                                AppConfig.KEY_BACK,
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        aDialog.dismiss();
                                        finish();
                                    }
                                },
                                false);
                    } else {

                        //permission is denied  and never ask again is checked
                        //shouldShowRequestPermissionRationale will false
                        //Ask user to go to settings and manually allow permission

                        showPermissionDialog(AppConfig.KEY_EMPTY_STRING,
                                "You have denied some permission. Allow all permission at " +
                                        "[Settings] -> [Permission]",
                                AppConfig.KEY_GO_SETTING,
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        //Go to settings
                                        Intent intent = new Intent(
                                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts(AppConfig.KEY_PKG, getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                },
                                AppConfig.KEY_BACK,
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        aDialog.dismiss();
                                        PlumbingActivity.this.finish();
                                    }
                                },
                                false);
                    }

                }
            }
        }

    }


    ///////////////////////////////////////////////////////////////////////////
    // Activity Methods
    ///////////////////////////////////////////////////////////////////////////


    //Method : Checking Internet
    private boolean isInternet() {
        return new ConnectionDetector(PlumbingActivity.this).isInternetConnected();
    }

    //Method : Initialisation
    private void init() {

        //Init Pref
        prefManager = new PrefManager(PlumbingActivity.this);

        //Setting up recyclerView
        a.recyclerView.setLayoutManager(new LinearLayoutManager(PlumbingActivity.this));
        a.recyclerView.setItemAnimator(new DefaultItemAnimator());
        a.recyclerView.setFocusable(false);

        //api
        if (isInternet()) {
            apiCity();
        } else {
            Toast.makeText(PlumbingActivity.this, "Please, Check your internet connection !!", Toast.LENGTH_SHORT).show();
        }

        //Listener
        a.lblCityName.setOnClickListener(listenerCity);
        a.imgBack.setOnClickListener(listenerBack);
    }

    //Method : Check And Request Permission For Calling
    private boolean checkAndRequestPermission() {

        List<String> listPermissionNeeded = new ArrayList<>();

        //Check All Permission Are Granted On Not
        for (String perm : appPermissions) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionNeeded.add(perm);
            }
        }

        //Ask For Non Granted Permission
        if (!listPermissionNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionNeeded.toArray(new String[listPermissionNeeded.size()]),
                    PERMISSION_REQUEST_CODE);
            return false;
        }
        return true;
    }

    //Method : Showing Dialog Permission
    public void showPermissionDialog(String title, String msg, String positiveLabel,
                                     View.OnClickListener positiveOnClick,
                                     String negativeLabel,
                                     View.OnClickListener negativeOnClick,
                                     boolean isCancelable) {

        @SuppressLint("InflateParams") View view = LayoutInflater
                .from(PlumbingActivity.this)
                .inflate(R.layout.dialog_need_permission, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(PlumbingActivity.this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setView(view);

        //Casting
        TextView lblMessage = view.findViewById(R.id.lblMessage);
        TextView lblPos = view.findViewById(R.id.lblPositiveButton);
        TextView lblNeg = view.findViewById(R.id.lblNegativeButton);

        //Setting Up Text
        lblMessage.setVisibility(View.GONE);
        lblPos.setText(positiveLabel);
        lblNeg.setText(negativeLabel);

        //Setting Up Listener
        lblPos.setOnClickListener(positiveOnClick);
        lblNeg.setOnClickListener(negativeOnClick);

        aDialog = builder.create();
        aDialog.show();
    }

    //Method : Call
    public void call(String mobileNo) {
        Toast.makeText(this, "Calling", Toast.LENGTH_SHORT).show();
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + mobileNo));
        if (ActivityCompat.checkSelfPermission(PlumbingActivity.this, Manifest.permission.CALL_PHONE)
                == PackageManager.PERMISSION_GRANTED) {
            PlumbingActivity.this.startActivity(callIntent);
        } else {
            Toast.makeText(this, "Please, Grant Permission To Call From Settings.", Toast.LENGTH_SHORT).show();
        }
    }


    ///////////////////////////////////////////////////////////////////////////
    // Listeners
    ///////////////////////////////////////////////////////////////////////////

    //Listener : City
    private View.OnClickListener listenerCity = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            CityFragment c = new CityFragment(listCity, new CityInterface() {
                @Override
                public void onCitySelected(String cityId, String cityName) {

                    a.lblCityName.setText(cityName);
                    a.lblCityName.setTag(cityId);

                    if (isInternet()) {
                        apiStores(cityId, cityName);
                    } else {
                        Toast.makeText(PlumbingActivity.this, "No Internet Connection !!", Toast.LENGTH_SHORT).show();
                    }

                }
            });
            c.show(getSupportFragmentManager(), "bottom_sheet");

        }
    };

    //Listener : Back
    private View.OnClickListener listenerBack = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };


    ///////////////////////////////////////////////////////////////////////////
    // API Implementation
    ///////////////////////////////////////////////////////////////////////////

    //Method : Api City List
    private void apiCity() {
        final ProgressDialog p = new ProgressDialog(PlumbingActivity.this);
        p.setMessage("Loading...");
        p.setCancelable(false);
        if (!p.isShowing()) {
            p.show();
        }

        ApiInterface apiInterface = APIClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.getCities();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                switch (response.code()) {

                    case 200:

                        try {
                            if (response.body() != null && response.isSuccessful()) {

                                String resBody = response.body().string();
                                JSONArray jsonArray = new JSONArray(resBody);

                                if (jsonArray.length() > 0) {

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject object = jsonArray.getJSONObject(i);

                                        String city = object.getString("title");
                                        String id = object.getString("id");

                                        City c = new City(id, city);
                                        listCity.add(c);
                                    }

                                    if (p.isShowing()) {
                                        p.dismiss();
                                    }
                                } else {
                                    if (p.isShowing()) {
                                        p.dismiss();
                                    }
                                }

                            } else {
                                if (p.isShowing()) {
                                    p.dismiss();
                                }
                            }
                        } catch (IOException | JSONException e) {
                            if (p.isShowing()) {
                                p.dismiss();
                            }
                        }
                        break;

                    case 401:
                    case 402:
                    case 403:
                    case 404:
                    case 405:
                    case 406:
                    case 407:
                    case 408:
                    case 409:
                    case 410:
                    case 501:
                    case 502:
                    case 503:
                    case 504:
                    case 505:
                    case 506:
                    case 507:
                    case 508:
                    case 509:
                    case 510:
                    default:
                        if (p.isShowing()) {
                            p.dismiss();
                        }

                        Toast.makeText(PlumbingActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if (p.isShowing()) {
                    p.dismiss();
                }

                Toast.makeText(PlumbingActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //Method : Api City List
    private void apiStores(final String cityId, final String cityName) {

        listMedical.clear();
        a.recyclerView.setAdapter(null);

        final ProgressDialog p = new ProgressDialog(PlumbingActivity.this);
        p.setMessage("Loading...");
        p.setCancelable(false);
        if (!p.isShowing()) {
            p.show();
        }

        ApiInterface apiInterface = APIClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.getPlum(cityId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                switch (response.code()) {

                    case 200:

                        try {
                            if (response.body() != null && response.isSuccessful()) {

                                String resBody = response.body().string();
                                JSONArray jsonArray = new JSONArray(resBody);

                                if (jsonArray.length() > 0) {

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject object = jsonArray.getJSONObject(i);

                                        String id = object.getString("id");
                                        String storeName = "પ્લંબિંગ";//object.getString("title");
                                        String owner1 = object.getString("name");
                                        String owner2 = "";//object.getString("owner_two");
                                        String contact1 = object.getString("contact_one");
                                        String contact2 = object.getString("contact_two");
                                        String address = cityName;//object.getString("address");

                                        Medical medical = new Medical(id, storeName, owner1, owner2, address, cityName, cityId, contact1, contact2);
                                        listMedical.add(medical);
                                    }

                                    if (p.isShowing()) {
                                        p.dismiss();
                                    }

                                    if (listMedical.size() > 0) {
                                        setAdapter();
                                    } else {
                                        Toast.makeText(PlumbingActivity.this, "No Cities Found !!", Toast.LENGTH_SHORT).show();
                                    }

                                } else {
                                    if (p.isShowing()) {
                                        p.dismiss();
                                    }
                                }

                            } else {
                                if (p.isShowing()) {
                                    p.dismiss();
                                }
                            }
                        } catch (IOException | JSONException e) {
                            if (p.isShowing()) {
                                p.dismiss();
                            }
                        }
                        break;

                    case 401:
                    case 402:
                    case 403:
                    case 404:
                    case 405:
                    case 406:
                    case 407:
                    case 408:
                    case 409:
                    case 410:
                    case 501:
                    case 502:
                    case 503:
                    case 504:
                    case 505:
                    case 506:
                    case 507:
                    case 508:
                    case 509:
                    case 510:
                    default:

                        if (p.isShowing()) {
                            p.dismiss();
                        }

                        Toast.makeText(PlumbingActivity.this, "Server Error", Toast.LENGTH_SHORT).show();

                        break;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if (p.isShowing()) {
                    p.dismiss();
                }

                Toast.makeText(PlumbingActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //API : Insert Data While Calling
    private void apiInsertData(String mobile1, String mobile2, String storeTitle, String cityId) {

        final String mStoreNo1 = mobile1;
        String mUserMobile = prefManager.getMobile();
        final String mStoreName = storeTitle;
        String mCityId = cityId;

        final ProgressDialog p = new ProgressDialog(PlumbingActivity.this);
        p.setMessage("Loading...");
        p.setCancelable(false);
        if (!p.isShowing()) {
            p.show();
        }

        Log.d(TAG, "apiInsertData: user_id:" + mUserMobile);
        Log.d(TAG, "apiInsertData: title:" + mStoreName);
        Log.d(TAG, "apiInsertData: contact_no:" + mStoreNo1);
        Log.d(TAG, "apiInsertData: type:" + "plumbing");
        Log.d(TAG, "apiInsertData: city_id:" + mCityId);

        ApiInterface apiInterface = APIClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.insertData(
                mUserMobile,
                mStoreName,
                mStoreNo1,
                "plumbing",
                mCityId
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                switch (response.code()) {

                    case 201:

                        try {

                            if (p.isShowing()) {
                                p.dismiss();
                            }

                            if (!TextUtils.isEmpty(mStoreNo1)) {
                                call(mStoreNo1);
                            } else {
                                Toast.makeText(PlumbingActivity.this, "Mobile No Is Not Available", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(PlumbingActivity.this, "Invalid Mobile No.", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case 401:
                    case 402:
                    case 403:
                    case 404:
                    case 405:
                    case 406:
                    case 407:
                    case 408:
                    case 409:
                    case 410:
                    case 501:
                    case 502:
                    case 503:
                    case 504:
                    case 505:
                    case 506:
                    case 507:
                    case 508:
                    case 509:
                    case 510:
                    default:

                        if (p.isShowing()) {
                            p.dismiss();
                        }

                        Toast.makeText(PlumbingActivity.this, "Server Error", Toast.LENGTH_SHORT).show();

                        break;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if (p.isShowing()) {
                    p.dismiss();
                }

                Toast.makeText(PlumbingActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //Method : Setting Kar Adapter
    private void setAdapter() {

        MedicalAdapter m = new MedicalAdapter(PlumbingActivity.this, listMedical, new MedicalInterface() {
            @Override
            public void onCall(String mobile1, String mobile2, String storeTitle, String cityId) {
                if (isInternet()) {
                    apiInsertData(mobile1, mobile2, storeTitle, cityId);
                } else {
                    Toast.makeText(PlumbingActivity.this, "Please, Check Your Internet Settings", Toast.LENGTH_SHORT).show();
                }
            }
        });
        a.recyclerView.setAdapter(m);

    }
}

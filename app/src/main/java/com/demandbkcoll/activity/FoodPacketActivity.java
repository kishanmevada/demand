package com.demandbkcoll.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.demandbkcoll.R;
import com.demandbkcoll.api.APIClient;
import com.demandbkcoll.bottomsheet.CityFragment;
import com.demandbkcoll.databinding.ActivityFoodPacketBinding;
import com.demandbkcoll.interfaces.ApiInterface;
import com.demandbkcoll.interfaces.CityInterface;
import com.demandbkcoll.model.City;
import com.demandbkcoll.utils.ConnectionDetector;
import com.demandbkcoll.utils.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FoodPacketActivity extends AppCompatActivity {

    private static final String TAG = "FoodPacketActivity";

    private ActivityFoodPacketBinding a;
    private PrefManager prefManager;
    private boolean isCitySelected = false;

    private ArrayList<City> listCity = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        a = DataBindingUtil.setContentView(this, R.layout.activity_food_packet);

        init();
    }

    ///////////////////////////////////////////////////////////////////////////
    // Activity Methods
    ///////////////////////////////////////////////////////////////////////////

    //Method : Init
    private void init() {

        //Hiding Action Bar
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        //Init Pref
        prefManager = new PrefManager(FoodPacketActivity.this);
        a.txtMobile.setText(prefManager.getMobile());

        //Listener
        a.lblSave.setOnClickListener(listenerSave);
        a.lblCityName.setOnClickListener(listenerCity);
        a.imgBack.setOnClickListener(listenerBack);

        //APIs
        if (isInternet()) {
            apiCity();
        } else {
            Toast.makeText(this, "Please, Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    //Method : Is Valid
    private boolean isValid() {

        if (!isCitySelected) {
            Toast.makeText(FoodPacketActivity.this, "Please, Select City", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(a.txtName.getText().toString().trim())) {
            Toast.makeText(FoodPacketActivity.this, "Please, Enter Your Name", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(a.txtMobile.getText().toString().trim())) {
            Toast.makeText(FoodPacketActivity.this, "Please, Enter Your Mobile", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(a.txtTotal.getText().toString().trim())) {
            Toast.makeText(FoodPacketActivity.this, "Please, Enter Total", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(a.txtAddress.getText().toString().trim())) {
            Toast.makeText(FoodPacketActivity.this, "Please, Enter Address", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    //Method : Is Internet
    private boolean isInternet() {
        return  new ConnectionDetector(FoodPacketActivity.this).isInternetConnected();
    }


    ///////////////////////////////////////////////////////////////////////////
    // Listener
    ///////////////////////////////////////////////////////////////////////////

    private View.OnClickListener listenerSave = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isInternet()) {
                if (isValid()) {
                    apiInsertReq();
                }
            }
        }
    };

    //Listener : City
    private View.OnClickListener listenerCity = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CityFragment c = new CityFragment(listCity, new CityInterface() {
                @Override
                public void onCitySelected(String cityId, String cityName) {
                    isCitySelected = true;
                    a.lblCityName.setText(cityName);
                    a.lblCityName.setTag(cityId);
                }
            });
            c.show(getSupportFragmentManager(), "bottom_sheet");

        }
    };

    private View.OnClickListener listenerBack = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isFinishing()) {
                finish();
            }
        }
    };



    ///////////////////////////////////////////////////////////////////////////
    // APIs
    ///////////////////////////////////////////////////////////////////////////


    //Method : Api City List
    private void apiCity() {
        final ProgressDialog p = new ProgressDialog(FoodPacketActivity.this);
        p.setMessage("Loading...");
        p.setCancelable(false);
        if (!p.isShowing()) {
            p.show();
        }

        ApiInterface apiInterface = APIClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.getCities();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                switch (response.code()) {

                    case 200:

                        try {
                            if (response.body() != null && response.isSuccessful()) {

                                String resBody = response.body().string();
                                JSONArray jsonArray = new JSONArray(resBody);

                                if (jsonArray.length() > 0) {

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject object = jsonArray.getJSONObject(i);

                                        String city = object.getString("title");
                                        String id = object.getString("id");

                                        City c = new City(id, city);
                                        listCity.add(c);
                                    }

                                    if (p.isShowing()) {
                                        p.dismiss();
                                    }
                                } else {
                                    if (p.isShowing()) {
                                        p.dismiss();
                                    }
                                }

                            } else {
                                if (p.isShowing()) {
                                    p.dismiss();
                                }
                            }
                        } catch (IOException | JSONException e) {
                            if (p.isShowing()) {
                                p.dismiss();
                            }
                        }
                        break;

                    case 401:
                    case 402:
                    case 403:
                    case 404:
                    case 405:
                    case 406:
                    case 407:
                    case 408:
                    case 409:
                    case 410:
                    case 501:
                    case 502:
                    case 503:
                    case 504:
                    case 505:
                    case 506:
                    case 507:
                    case 508:
                    case 509:
                    case 510:
                    default:

                        if (p.isShowing()) {
                            p.dismiss();
                        }

                        Toast.makeText(FoodPacketActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {

                if (p.isShowing()) {
                    p.dismiss();
                }

                Toast.makeText(FoodPacketActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    ///////////////////////////////////////////////////////////////////////////
    // APIs
    ///////////////////////////////////////////////////////////////////////////

    //Method : Api Insert Request
    private void apiInsertReq() {

        String name = a.txtName.getText().toString().trim();
        String mobile = a.txtMobile.getText().toString().trim();
        String address = a.txtAddress.getText().toString().trim();
        String total = a.txtTotal.getText().toString().trim();
        String userId = prefManager.getId();
        String cityId = String.valueOf(a.lblCityName.getTag());
        String req_type = "food_packet";

        Log.d(TAG, "apiInsertReq: user_id:"+userId);
        Log.d(TAG, "apiInsertReq: request_type:"+req_type);
        Log.d(TAG, "apiInsertReq: description:"+address);
        Log.d(TAG, "apiInsertReq: contact_no:"+mobile);
        Log.d(TAG, "apiInsertReq: no_of_people:"+total);
        Log.d(TAG, "apiInsertReq: name:"+name);
        Log.d(TAG, "apiInsertReq: city_id:"+cityId);

        final ProgressDialog p = new ProgressDialog(FoodPacketActivity.this);
        p.setMessage("Loading...");
        p.setCancelable(false);
        if (!p.isShowing()) {
            p.show();
        }

        ApiInterface apiInterface = APIClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.insertReq(
                userId, req_type, address, mobile, total, name, cityId);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                //Response Code
                Log.d(TAG, "onResponse: apiState " + response.code());

                switch (response.code()) {

                    case 201:

                        try {
                            if (response.body() != null && response.isSuccessful()) {

                                String resBody = response.body().string();
                                JSONObject object = new JSONObject(resBody);

                                showMessage();

                            } else {
                                if (p.isShowing()) {
                                    p.dismiss();
                                }
                            }
                        } catch (IOException | JSONException e) {
                            if (p.isShowing()) {
                                p.dismiss();
                            }
                        }
                        break;

                    case 401:
                    case 402:
                    case 403:
                    case 404:
                    case 405:
                    case 406:
                    case 407:
                    case 408:
                    case 409:
                    case 410:
                    case 501:
                    case 502:
                    case 503:
                    case 504:
                    case 505:
                    case 506:
                    case 507:
                    case 508:
                    case 509:
                    case 510:
                    default:
                        if (p.isShowing()) {
                            p.dismiss();
                        }

                        Toast.makeText(FoodPacketActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if (p.isShowing()) {
                    p.dismiss();
                }

                Toast.makeText(FoodPacketActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showMessage() {

        String title  = "નોંધ";
        String message = "આભાર !\n" +
                "આપનો મૅસેંજ અમને સફળતા પૂર્વક મળી ગયો છે. વધુ માહિતી માટે અમે તમને તમારા મોબાઈલ નંબર પર સંપર્ક કરીશું.";

        AlertDialog.Builder builder = new AlertDialog.Builder(FoodPacketActivity.this);
        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (!isFinishing()) {
                    finish();
                }
            }
        });

        AlertDialog a = builder.create();
        a.show();
    }

}

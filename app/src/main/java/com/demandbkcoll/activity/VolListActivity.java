package com.demandbkcoll.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.demandbkcoll.R;
import com.demandbkcoll.adapter.VallistAdapter;
import com.demandbkcoll.api.APIClient;
import com.demandbkcoll.bottomsheet.CityFragment;
import com.demandbkcoll.databinding.ActivityVolListBinding;
import com.demandbkcoll.interfaces.ApiInterface;
import com.demandbkcoll.interfaces.CityInterface;
import com.demandbkcoll.interfaces.VolInterface;
import com.demandbkcoll.model.City;
import com.demandbkcoll.model.Vollist;
import com.demandbkcoll.utils.AppConfig;
import com.demandbkcoll.utils.ConnectionDetector;
import com.demandbkcoll.utils.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VolListActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 10;

    private static final float maxHeight = 1280.0f;
    private static final float maxWidth = 1280.0f;

    private AlertDialog aDialog;
    private ActivityVolListBinding a;
    private PrefManager prefManager;

    private String reqType = "";

    private List<Vollist> listVollist = new ArrayList<>();
    private List<City> listCity = new ArrayList<>();

    String[] appPermissions = {Manifest.permission.CALL_PHONE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        a = DataBindingUtil.setContentView(this, R.layout.activity_vol_list);

        if (checkAndRequestPermission()) {
            init();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == PERMISSION_REQUEST_CODE) {

            //Init
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;

            //Gather permission grant Result
            for (int i = 0; i < grantResults.length; i++) {

                //Add Only denied permission
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }

            }

            //Check if all permission are granted
            if (deniedCount == 0) {

                //Allow to proceed
                init();
            } else {

                //At least one or all permission are are denied

                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {

                    String permName = entry.getKey();

                    //Permission is denied and never ask again isn't checked
                    //ask again explaining the importance of permission
                    //shouldShowRequestPermissionRationale will true
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {

                        showPermissionDialog(AppConfig.KEY_EMPTY_STRING,
                                "App need your permission to process further",
                                AppConfig.KEY_GRANT_PERMISSION,
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        aDialog.dismiss();
                                        checkAndRequestPermission();
                                    }
                                },
                                AppConfig.KEY_BACK,
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        aDialog.dismiss();
                                        finish();
                                    }
                                },
                                false);
                    } else {

                        //permission is denied  and never ask again is checked
                        //shouldShowRequestPermissionRationale will false
                        //Ask user to go to settings and manually allow permission

                        showPermissionDialog(AppConfig.KEY_EMPTY_STRING,
                                "You have denied some permission. Allow all permission at " +
                                        "[Settings] -> [Permission]",
                                AppConfig.KEY_GO_SETTING,
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        //Go to settings
                                        Intent intent = new Intent(
                                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts(AppConfig.KEY_PKG, getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                },
                                AppConfig.KEY_BACK,
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        aDialog.dismiss();
                                        VolListActivity.this.finish();
                                    }
                                },
                                false);
                    }

                }
            }
        }

    }


    ///////////////////////////////////////////////////////////////////////////
    // Activity Methods
    ///////////////////////////////////////////////////////////////////////////


    //Method : Checking Internet
    private boolean isInternet() {
        return new ConnectionDetector(VolListActivity.this).isInternetConnected();
    }

    //Method : Initialisation
    private void init() {

        prefManager = new PrefManager(VolListActivity.this);
        reqType = prefManager.getInputType();

        if (reqType.equalsIgnoreCase("kar_kit")) {
            a.lblTitle.setText("કરિયાણા કિટ");
        } else if (reqType.equalsIgnoreCase("food_packet")) {
            a.lblTitle.setText("ફૂડ પેકેટ");
        }

        //Setting up recyclerView
        a.recyclerView.setLayoutManager(new LinearLayoutManager(VolListActivity.this));
        a.recyclerView.setItemAnimator(new DefaultItemAnimator());
        a.recyclerView.setFocusable(false);

        //api
        if (isInternet()) {
            apiCity();
        } else {
            Toast.makeText(VolListActivity.this, "Please, Check your internet connection !!", Toast.LENGTH_SHORT).show();
        }

        //Listener
        a.lblCityName.setOnClickListener(listenerCity);
        a.imgBack.setOnClickListener(listenerBack);
    }

    //Method : Check And Request Permission For Calligng
    private boolean checkAndRequestPermission() {

        List<String> listPermissionNeeded = new ArrayList<>();

        //Check All Permission Are Granted On Not
        for (String perm : appPermissions) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionNeeded.add(perm);
            }
        }

        //Ask For Non Granted Permission
        if (!listPermissionNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionNeeded.toArray(new String[listPermissionNeeded.size()]),
                    PERMISSION_REQUEST_CODE);
            return false;
        }
        return true;
    }

    //Method : Showing Dialog Permission
    public void showPermissionDialog(String title, String msg, String positiveLabel,
                                     View.OnClickListener positiveOnClick,
                                     String negativeLabel,
                                     View.OnClickListener negativeOnClick,
                                     boolean isCancelable) {

        @SuppressLint("InflateParams") View view = LayoutInflater
                .from(VolListActivity.this)
                .inflate(R.layout.dialog_need_permission, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(VolListActivity.this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setView(view);

        //Casting
        TextView lblMessage = view.findViewById(R.id.lblMessage);
        TextView lblPos = view.findViewById(R.id.lblPositiveButton);
        TextView lblNeg = view.findViewById(R.id.lblNegativeButton);

        //Setting Up Text
        lblMessage.setVisibility(View.GONE);
        lblPos.setText(positiveLabel);
        lblNeg.setText(negativeLabel);

        //Setting Up Listener
        lblPos.setOnClickListener(positiveOnClick);
        lblNeg.setOnClickListener(negativeOnClick);

        aDialog = builder.create();
        aDialog.show();
    }


    ///////////////////////////////////////////////////////////////////////////
    // Listeners
    ///////////////////////////////////////////////////////////////////////////

    //Listener : City
    private View.OnClickListener listenerCity = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            CityFragment c = new CityFragment(listCity, new CityInterface() {
                @Override
                public void onCitySelected(String cityId, String cityName) {

                    a.lblCityName.setText(cityName);
                    a.lblCityName.setTag(cityId);

                    if (isInternet()) {
                        apiGetReq(cityId, cityName);
                    } else {
                        Toast.makeText(VolListActivity.this, "No Internet Connection !!", Toast.LENGTH_SHORT).show();
                    }

                }
            });
            c.show(getSupportFragmentManager(), "bottom_sheet");

        }
    };

    //Listener : Back
    private View.OnClickListener listenerBack = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };


    ///////////////////////////////////////////////////////////////////////////
    // API Implementation
    ///////////////////////////////////////////////////////////////////////////

    //Method : Api City List
    private void apiCity() {
        final ProgressDialog p = new ProgressDialog(VolListActivity.this);
        p.setMessage("Loading...");
        p.setCancelable(false);
        if (!p.isShowing()) {
            p.show();
        }

        ApiInterface apiInterface = APIClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.getCities();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                switch (response.code()) {

                    case 200:

                        try {
                            if (response.body() != null && response.isSuccessful()) {

                                String resBody = response.body().string();
                                JSONArray jsonArray = new JSONArray(resBody);

                                if (jsonArray.length() > 0) {

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject object = jsonArray.getJSONObject(i);

                                        String city = object.getString("title");
                                        String id = object.getString("id");

                                        City c = new City(id, city);
                                        listCity.add(c);
                                    }

                                    if (p.isShowing()) {
                                        p.dismiss();
                                    }
                                } else {
                                    if (p.isShowing()) {
                                        p.dismiss();
                                    }
                                }

                            } else {
                                if (p.isShowing()) {
                                    p.dismiss();
                                }
                            }
                        } catch (IOException | JSONException e) {
                            if (p.isShowing()) {
                                p.dismiss();
                            }
                        }
                        break;

                    case 401:
                    case 402:
                    case 403:
                    case 404:
                    case 405:
                    case 406:
                    case 407:
                    case 408:
                    case 409:
                    case 410:
                    case 501:
                    case 502:
                    case 503:
                    case 504:
                    case 505:
                    case 506:
                    case 507:
                    case 508:
                    case 509:
                    case 510:
                    default:
                        if (p.isShowing()) {
                            p.dismiss();
                        }

                        Toast.makeText(VolListActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if (p.isShowing()) {
                    p.dismiss();
                }

                Toast.makeText(VolListActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //Method : Done Req
    private void apiDoneReq(String id) {

        final ProgressDialog p = new ProgressDialog(VolListActivity.this);
        p.setMessage("Loading...");
        p.setCancelable(false);
        if (!p.isShowing()) {
            p.show();
        }

        ApiInterface apiInterface = APIClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.updateStatus(id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                switch (response.code()) {

                    case 200:

                        try {
                            if (response.body() != null && response.isSuccessful()) {

                                String resBody = response.body().string();

                                JSONObject object = new JSONObject(resBody);

                                if (object.getString("status").equalsIgnoreCase("1")) {
                                    Toast.makeText(VolListActivity.this, "પૂર્ણ", Toast.LENGTH_SHORT).show();

                                    String cityName = a.lblCityName.getText().toString().trim();
                                    String cityId = String.valueOf(a.lblCityName.getTag());
                                    apiGetReq(cityId, cityName);
                                }

                                if (p.isShowing()) {
                                    p.dismiss();
                                }

                            } else {
                                if (p.isShowing()) {
                                    p.dismiss();
                                }
                            }
                        } catch (IOException | JSONException e) {
                            if (p.isShowing()) {
                                p.dismiss();
                            }
                        }
                        break;

                    case 401:
                    case 402:
                    case 403:
                    case 404:
                    case 405:
                    case 406:
                    case 407:
                    case 408:
                    case 409:
                    case 410:
                    case 501:
                    case 502:
                    case 503:
                    case 504:
                    case 505:
                    case 506:
                    case 507:
                    case 508:
                    case 509:
                    case 510:
                    default:
                        if (p.isShowing()) {
                            p.dismiss();
                        }

                        Toast.makeText(VolListActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if (p.isShowing()) {
                    p.dismiss();
                }

                Toast.makeText(VolListActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //Method : Api City List
    private void apiGetReq(final String cityId, final String cityName) {

        listVollist.clear();
        a.recyclerView.setAdapter(null);

        final ProgressDialog p = new ProgressDialog(VolListActivity.this);
        p.setMessage("Loading...");
        p.setCancelable(false);
        if (!p.isShowing()) {
            p.show();
        }

        ApiInterface apiInterface = APIClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.getReqs(cityId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                switch (response.code()) {

                    case 200:

                        try {
                            if (response.body() != null && response.isSuccessful()) {

                                String resBody = response.body().string();
                                JSONArray jsonArray = new JSONArray(resBody);

                                if (jsonArray.length() > 0) {

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject object = jsonArray.getJSONObject(i);

                                        String id = object.getString("id");
                                        String userId = object.getString("user_id");
                                        String name = object.getString("name");
                                        String requestType = object.getString("request_type");
                                        String description = object.getString("description");
                                        String contactNo = object.getString("contact_no");
                                        String noOfPeople = object.getString("no_of_people");
                                        String completed = object.getString("completed");

                                        Vollist v = new Vollist(
                                                id,
                                                userId,
                                                name,
                                                requestType,
                                                description,
                                                contactNo,
                                                noOfPeople,
                                                completed
                                        );

                                        if (requestType.equalsIgnoreCase(reqType)) {
                                            listVollist.add(v);
                                        }
                                    }

                                    if (p.isShowing()) {
                                        p.dismiss();
                                    }

                                    if (listVollist.size() > 0) {
                                        setAdapter();
                                    } else {
                                        Toast.makeText(VolListActivity.this, "કોઈ વિનંતી મળતી નથી.", Toast.LENGTH_SHORT).show();
                                    }

                                } else {
                                    if (p.isShowing()) {
                                        p.dismiss();
                                    }
                                }

                            } else {
                                if (p.isShowing()) {
                                    p.dismiss();
                                }
                            }
                        } catch (IOException | JSONException e) {
                            if (p.isShowing()) {
                                p.dismiss();
                            }
                        }
                        break;

                    case 401:
                    case 402:
                    case 403:
                    case 404:
                    case 405:
                    case 406:
                    case 407:
                    case 408:
                    case 409:
                    case 410:
                    case 501:
                    case 502:
                    case 503:
                    case 504:
                    case 505:
                    case 506:
                    case 507:
                    case 508:
                    case 509:
                    case 510:
                    default:

                        if (p.isShowing()) {
                            p.dismiss();
                        }

                        Toast.makeText(VolListActivity.this, "Server Error", Toast.LENGTH_SHORT).show();

                        break;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if (p.isShowing()) {
                    p.dismiss();
                }

                Toast.makeText(VolListActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //Method : Setting Kar Adapter
    private void setAdapter() {

        VallistAdapter k = new VallistAdapter(VolListActivity.this, listVollist, new VolInterface() {
            @Override
            public void onDone(final String id) {

                AlertDialog.Builder b = new AlertDialog.Builder(VolListActivity.this);
                b.setCancelable(false);
                b.setTitle("Note");
                b.setMessage(" શું તમે ખરેખર આ વિનંતી પૂર્ણ કરવા માગો છો ?");
                b.setPositiveButton("હા", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                        if (isInternet()) {
                            apiDoneReq(id);
                        } else {
                            Toast.makeText(VolListActivity.this, "Please, Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                b.setNegativeButton("ના", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alertDialog = b.create();
                alertDialog.show();
            }
        });
        a.recyclerView.setAdapter(k);

    }
}

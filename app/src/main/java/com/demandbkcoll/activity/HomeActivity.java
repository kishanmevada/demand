package com.demandbkcoll.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.demandbkcoll.R;
import com.demandbkcoll.api.APIClient;
import com.demandbkcoll.databinding.ActivityHomeBinding;
import com.demandbkcoll.interfaces.ApiInterface;
import com.demandbkcoll.model.City;
import com.demandbkcoll.utils.ConnectionDetector;
import com.demandbkcoll.utils.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    private ActivityHomeBinding a;
    private PrefManager prefManager;
    private String appVersion = "1.0.2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        a = DataBindingUtil.setContentView(this, R.layout.activity_home);

        init();
    }

    private void init() {

        //Init Pref
        prefManager = new PrefManager(HomeActivity.this);

        if (prefManager.isFirstTime().equals("True")) {
            prefManager.setFirstTime("False");
            prefManager.setLogin("False");
            startActivity(new Intent(HomeActivity.this, LoginActivity.class));
            Toast.makeText(HomeActivity.this, "Please, Re-Login !!", Toast.LENGTH_SHORT).show();
            finish();
        }

        if (prefManager.getLoginType().equalsIgnoreCase("Cust")) {

            a.linearCust.setVisibility(View.VISIBLE);
            a.lblUnHelped.setVisibility(View.VISIBLE);
            a.lblThree.setVisibility(View.VISIBLE);
            a.linearEnd.setVisibility(View.VISIBLE);

            //Listener
            a.linearKariyanu.setOnClickListener(listenerKariyanu);
            a.linearMedical.setOnClickListener(listenerMedical);
            a.linearFoodPacket.setOnClickListener(listenerFoodPacket);
            a.linearKiranaKit.setOnClickListener(listenerKariyanuKit);
            a.linearElectronic.setOnClickListener(listenerEle);
            a.linearPlumbing.setOnClickListener(listenerPlumb);
            a.linearVadil.setOnClickListener(listenerVadil);
            a.linearJati.setOnClickListener(linearJati);
            a.linearPsyDoctors.setOnClickListener(linearDocPsy);
            a.linearDoctors.setOnClickListener(linearDoc);

        } else {

            a.linearCust.setVisibility(View.GONE);
            a.lblUnHelped.setVisibility(View.GONE);
            a.lblThree.setVisibility(View.GONE);
            a.linearEnd.setVisibility(View.GONE);

            a.linearFoodPacket.setOnClickListener(listenerFoodPacketVol);
            a.linearKiranaKit.setOnClickListener(listenerKariyanuKitVol);

        }

        if (isInternet()) {
            apiVersion();
        } else {
            Toast.makeText(HomeActivity.this, "Please, Check Your Internet Connection !!", Toast.LENGTH_SHORT).show();
        }
    }

    //Method : Api City List
    private void apiVersion() {
        final ProgressDialog p = new ProgressDialog(HomeActivity.this);
        p.setMessage("Loading...");
        p.setCancelable(false);
        if (!p.isShowing()) {
            p.show();
        }

        ApiInterface apiInterface = APIClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.getVersion();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                switch (response.code()) {

                    case 201:

                        try {
                            if (response.body() != null && response.isSuccessful()) {

                                String resBody = response.body().string();

                                JSONObject object = new JSONObject(resBody);

                                String version = object.getString("version");
                                String url = object.getString("url");

                                if (!version.equalsIgnoreCase(appVersion)) {
                                    showMessage(url);
                                }

                                if (p.isShowing()) {
                                    p.dismiss();
                                }

                            } else {
                                if (p.isShowing()) {
                                    p.dismiss();
                                }
                            }
                        } catch (IOException | JSONException e) {
                            if (p.isShowing()) {
                                p.dismiss();
                            }
                        }
                        break;

                    case 401:
                    case 402:
                    case 403:
                    case 404:
                    case 405:
                    case 406:
                    case 407:
                    case 408:
                    case 409:
                    case 410:
                    case 501:
                    case 502:
                    case 503:
                    case 504:
                    case 505:
                    case 506:
                    case 507:
                    case 508:
                    case 509:
                    case 510:
                    default:
                        if (p.isShowing()) {
                            p.dismiss();
                        }

                        Toast.makeText(HomeActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if (p.isShowing()) {
                    p.dismiss();
                }

                Toast.makeText(HomeActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showMessage(final String url) {

        String title  = "નોંધ";
        String message = "આભાર !\n" +
                "એપ નવા ફીચર સાથે અપડેટ કરવામાં આવેલી છે. 'અપડેટ' પર ક્લિક કરીને એપ ને અપડેટ કરો. નવી એપ ને ઇન્સ્ટોલ કરો.";

        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton("અપડેટ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (!isFinishing()) {
                    try {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);
                    } catch (Exception e) {
                        Toast.makeText(HomeActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog a = builder.create();
        a.show();
    }

    //Method : Checking Internet
    private boolean isInternet() {
        return new ConnectionDetector(HomeActivity.this).isInternetConnected();
    }

    //Listener : Kariyanu
    private View.OnClickListener listenerKariyanu = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isFinishing()) {
                startActivity(new Intent(HomeActivity.this, KarActivity.class));
            }
        }
    };

    //Listener : Medical
    private View.OnClickListener listenerMedical = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isFinishing()) {
                startActivity(new Intent(HomeActivity.this, MedicalActivity.class));
            }
        }
    };

    //Listener : Food Packet
    private View.OnClickListener listenerFoodPacket = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isFinishing()) {
                startActivity(new Intent(HomeActivity.this, FoodPacketActivity.class));
            }
        }
    };

    //Listener : Kariyanu Kit
    private View.OnClickListener listenerKariyanuKit = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isFinishing()) {
                startActivity(new Intent(HomeActivity.this, KarKitActivity.class));
            }
        }
    };

    private View.OnClickListener listenerFoodPacketVol = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isFinishing()) {
                prefManager.setInputType("food_packet");
                startActivity(new Intent(HomeActivity.this, VolListActivity.class));
            }
        }
    };

    private View.OnClickListener listenerKariyanuKitVol = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isFinishing()) {
                prefManager.setInputType("kar_kit");
                startActivity(new Intent(HomeActivity.this, VolListActivity.class));
            }
        }
    };

    private View.OnClickListener listenerEle = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isFinishing()) {
                startActivity(new Intent(HomeActivity.this, ElectricActivity.class));
            }
        }
    };

    private View.OnClickListener listenerPlumb = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isFinishing()) {
                startActivity(new Intent(HomeActivity.this, PlumbingActivity.class));
            }
        }
    };

    private View.OnClickListener listenerVadil = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isFinishing()) {
                startActivity(new Intent(HomeActivity.this, VadilActivity.class));
            }
        }
    };

    private View.OnClickListener linearJati = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isFinishing()) {
                startActivity(new Intent(HomeActivity.this, JatiActivity.class));
            }
        }
    };

    private View.OnClickListener linearDocPsy = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isFinishing()) {
                startActivity(new Intent(HomeActivity.this, DistDoctorsActivity.class));
            }
        }
    };

    private View.OnClickListener linearDoc = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isFinishing()) {
                startActivity(new Intent(HomeActivity.this, DoctorsActivity.class));
            }
        }
    };
}

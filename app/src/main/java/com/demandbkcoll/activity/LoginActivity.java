package com.demandbkcoll.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.demandbkcoll.R;
import com.demandbkcoll.api.APIClient;
import com.demandbkcoll.databinding.ActivityLoginBinding;
import com.demandbkcoll.interfaces.ApiInterface;
import com.demandbkcoll.utils.ConnectionDetector;
import com.demandbkcoll.utils.PrefManager;
import com.demandbkcoll.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    private ActivityLoginBinding a;
    private boolean isVol = false;
    private PrefManager prefManager;

    ///////////////////////////////////////////////////////////////////////////
    // Override Methods
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.hideStatusBar(LoginActivity.this);
        a = DataBindingUtil.setContentView(this, R.layout.activity_login);

        init();
    }


    ///////////////////////////////////////////////////////////////////////////
    // Activity Method
    ///////////////////////////////////////////////////////////////////////////

    //Method : Init
    private void init() {

        //Hiding Action Bar
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        //Init Pref
        prefManager = new PrefManager(LoginActivity.this);

        //Starting Home If Already Logged In
        if (prefManager.isLogin().equalsIgnoreCase("True")) {
            launchHome();
        }

        //Listener
        a.lblCust.setOnClickListener(listenerCust);
        a.lblVol.setOnClickListener(listenerVol);
        a.lblLogin.setOnClickListener(listenerLogin);
        a.txtMobile.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:

                            if (isVol) {
                                a.txtPassword.requestFocus();
                                a.txtPassword.setFocusable(true);
                                a.txtPassword.setCursorVisible(true);
                            } else {
                                login();
                            }

                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

    }

    //Method : Internet Connection Checking
    private boolean isInternet() {
        return new ConnectionDetector(LoginActivity.this).isInternetConnected();
    }

    private void login() {
        if (isInternet()) {
            if (!TextUtils.isEmpty(a.txtMobile.getText().toString().trim())) {
                apiLogin();
            }else {
                Toast.makeText(LoginActivity.this, "Please, Enter Mobile No", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(LoginActivity.this, "Please, Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    ///////////////////////////////////////////////////////////////////////////
    // Listener
    ///////////////////////////////////////////////////////////////////////////

    //Listener : Customer Login
    private View.OnClickListener listenerCust = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isVol = false;
            a.lblCust.setBackground(LoginActivity.this.getResources().getDrawable(R.drawable.bg_grey));
            a.lblVol.setBackgroundColor(Color.TRANSPARENT);
            a.lblCust.setTextColor(Color.BLACK);
            a.lblVol.setTextColor(Color.parseColor("#ACACAC"));
            a.linearPassword.setVisibility(View.GONE);
        }
    };

    //Listener : Volunteer login
    private View.OnClickListener listenerVol = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isVol = true;
            a.lblCust.setBackgroundColor(Color.TRANSPARENT);
            a.lblVol.setBackground(LoginActivity.this.getResources().getDrawable(R.drawable.bg_grey));
            a.lblVol.setTextColor(Color.BLACK);
            a.lblCust.setTextColor(Color.parseColor("#ACACAC"));
            a.linearPassword.setVisibility(View.VISIBLE);
        }
    };

    //Listener : Login
    private View.OnClickListener listenerLogin = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            login();
        }
    };


    ///////////////////////////////////////////////////////////////////////////
    // APIs
    ///////////////////////////////////////////////////////////////////////////

    //API : Login
    private void apiLogin() {

        final ProgressDialog p = new ProgressDialog(LoginActivity.this);
        p.setMessage("Loading...");
        p.setCancelable(false);
        if (!p.isShowing()) {
            p.show();
        }

        final String mobile;
        String password;
        Call<ResponseBody> call = null;
        ApiInterface apiInterface = APIClient.getClient().create(ApiInterface.class);

        if (isVol) {
            mobile = a.txtMobile.getText().toString().trim();
            password = a.txtPassword.getText().toString().trim();
            call = apiInterface.loginVol(mobile, password);
        } else {
            mobile = a.txtMobile.getText().toString().trim();
            call = apiInterface.login(mobile);
        }
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                //Response Code
                Log.d(TAG, "onResponse: apiState " + response.code());

                switch (response.code()) {

                    case 200:

                        if (isVol) {
                            try {
                                if (response.body() != null && response.isSuccessful()) {

                                    String resBody = response.body().string();
                                    JSONObject object = new JSONObject(resBody);

                                    if (object.has("status")) {

                                        //Setting Up Prefs
                                        prefManager.setMobile(mobile);
                                        prefManager.setLogin("True");
                                        prefManager.setLoginType("Vol");

                                        if (p.isShowing()) {
                                            p.dismiss();
                                        }

                                        //Staring Activity
                                        launchHome();
                                    }

                                } else {
                                    if (p.isShowing()) {
                                        p.dismiss();
                                    }
                                }
                            } catch (IOException | JSONException e) {
                                if (p.isShowing()) {
                                    p.dismiss();
                                }
                            }
                        } else {
                            try {
                                if (response.body() != null && response.isSuccessful()) {

                                    String resBody = response.body().string();
                                    JSONObject object = new JSONObject(resBody);

                                    if (object.has("mobile")) {

                                        //Getting Token
                                        String mobile = object.getString("mobile");
                                        String id = object.getString("id");

                                        //Setting Up Prefs
                                        prefManager.setMobile(mobile);
                                        prefManager.setId(id);
                                        prefManager.setLogin("True");
                                        prefManager.setLoginType("Cust");

                                        if (p.isShowing()) {
                                            p.dismiss();
                                        }

                                        //Staring Activity
                                        launchHome();
                                    }

                                } else {
                                    if (p.isShowing()) {
                                        p.dismiss();
                                    }
                                }
                            } catch (IOException | JSONException e) {
                                if (p.isShowing()) {
                                    p.dismiss();
                                }
                            }
                        }
                        break;

                    case 401:
                    case 402:
                    case 403:
                    case 404:
                    case 405:
                    case 406:
                    case 407:
                    case 408:
                    case 409:
                    case 410:
                    case 501:
                    case 502:
                    case 503:
                    case 504:
                    case 505:
                    case 506:
                    case 507:
                    case 508:
                    case 509:
                    case 510:
                    default:
                        if (p.isShowing()) {
                            p.dismiss();
                        }

                        Toast.makeText(LoginActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if (p.isShowing()) {
                    p.dismiss();
                }

                Toast.makeText(LoginActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void launchHome() {
        if (!isFinishing()) {
            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
            LoginActivity.this.startActivity(intent);
            LoginActivity.this.finish();
        }
    }


}

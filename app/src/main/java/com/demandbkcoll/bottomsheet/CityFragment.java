package com.demandbkcoll.bottomsheet;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.demandbkcoll.R;
import com.demandbkcoll.adapter.CityAdapter;
import com.demandbkcoll.databinding.FragmentCityBinding;
import com.demandbkcoll.interfaces.CityInterface;
import com.demandbkcoll.model.City;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.List;

public class CityFragment extends BottomSheetDialogFragment {

    private List<City> listCity;
    private CityInterface cityInterface;

    public CityFragment(List<City> listCity, CityInterface cityInterface) {
        this.listCity = listCity;
        this.cityInterface = cityInterface;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentCityBinding f = DataBindingUtil.inflate(inflater, R.layout.fragment_city, container, false);
        View view = f.getRoot();

        f.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        f.recyclerView.setItemAnimator(new DefaultItemAnimator());
        f.recyclerView.setFocusable(false);

        CityAdapter c = new CityAdapter(getActivity(), listCity, new CityInterface() {
            @Override
            public void onCitySelected(String cityId, String cityName) {
                cityInterface.onCitySelected(cityId, cityName);
                dismiss();
            }
        });
        f.recyclerView.setAdapter(c);

        return view;
    }
}
